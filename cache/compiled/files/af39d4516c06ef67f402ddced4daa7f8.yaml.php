<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/kylemobilia/Documents/MEO_WEBSITE_BITBUCKET/maison9/website/user/themes/maison9/blueprints/default.yaml',
    'modified' => 1539177095,
    'data' => [
        'extends@' => 'default',
        'form' => [
            'fields' => [
                'tabs' => [
                    'fields' => [
                        'advanced' => [
                            'fields' => [
                                'columns' => [
                                    'fields' => [
                                        'column1' => [
                                            'fields' => [
                                                'header.body_classes' => [
                                                    'markdown' => true,
                                                    'description' => ''
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        'metadata' => [
                            'type' => 'tab',
                            'title' => 'Meta Données',
                            'fields' => [
                                'header.meta_title' => [
                                    'type' => 'text',
                                    'label' => 'Meta Title'
                                ],
                                'header.meta_description' => [
                                    'type' => 'textarea',
                                    'label' => 'Meta description'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
];
