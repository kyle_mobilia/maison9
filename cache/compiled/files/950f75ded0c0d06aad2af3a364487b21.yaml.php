<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/home/clients/1b0a8e5a9f435d385ae4cefa98dfc9b7/web/user/accounts/meo.yaml',
    'modified' => 1539949023,
    'data' => [
        'email' => 'webdev@meomeo.ch',
        'fullname' => 'Louis Paschoud',
        'title' => 'Administrator',
        'state' => 'enabled',
        'access' => [
            'admin' => [
                'super' => 'true',
                'login' => 'true'
            ],
            'site' => [
                'login' => 'true'
            ]
        ],
        'hashed_password' => '$2y$10$28/FrhEiU7Y6jVaEyvHpt.NeWj4qSeDiKEmS7OTPhudz8iIzDapvu',
        'language' => 'en',
        'twofa_enabled' => false,
        'twofa_secret' => 'P4OVZBCJTBJRBAXX6S7ZY5Q6ND2AGNS5'
    ]
];
