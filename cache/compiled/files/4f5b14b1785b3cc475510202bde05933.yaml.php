<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/home/clients/1b0a8e5a9f435d385ae4cefa98dfc9b7/web/user/themes/maison9/blueprints/landing-page.yaml',
    'modified' => 1540283905,
    'data' => [
        'title' => 'Landing Page',
        'extends@' => 'default',
        'form' => [
            'fields' => [
                'tabs' => [
                    'fields' => [
                        'content' => [
                            'fields' => [
                                'header.slideshow' => [
                                    'name' => 'slideshow',
                                    'type' => 'list',
                                    'style' => 'vertical',
                                    'label' => 'Slideshow',
                                    'fields' => [
                                        '.slide' => [
                                            'type' => 'filepicker',
                                            'folder' => '@self',
                                            'preview_images' => true,
                                            'label' => 'Select a file'
                                        ],
                                        '.slide_duration' => [
                                            'name' => 'slide duration',
                                            'label' => 'Durée de la slide en secondes',
                                            'type' => 'text',
                                            'default' => 2,
                                            'validate' => [
                                                'type' => 'int',
                                                'max' => 60
                                            ]
                                        ],
                                        '.show_txt' => [
                                            'type' => 'toggle',
                                            'label' => 'Afficher le texte sur cette slide',
                                            'highlight' => 1,
                                            'default' => 1,
                                            'options' => [
                                                1 => 'Oui',
                                                0 => 'Non'
                                            ]
                                        ],
                                        '.is_movie' => [
                                            'type' => 'toggle',
                                            'label' => 'Slide Vidéo',
                                            'highlight' => 1,
                                            'default' => 0,
                                            'options' => [
                                                1 => 'Oui',
                                                0 => 'Non'
                                            ]
                                        ],
                                        '.movie_color' => [
                                            'type' => 'colorpicker',
                                            'label' => 'Choisir la couleur du background'
                                        ],
                                        '.movie_pictures' => [
                                            'type' => 'filepicker',
                                            'folder' => '@self',
                                            'preview_images' => true,
                                            'label' => 'Sélectionner le preview de la vidéo'
                                        ],
                                        '.logo' => [
                                            'type' => 'filepicker',
                                            'folder' => '@self',
                                            'preview_images' => true,
                                            'label' => 'Sélectionner le logo du slide'
                                        ]
                                    ]
                                ],
                                'header.slideshow_txt' => [
                                    'name' => 'slideshow_txt',
                                    'type' => 'list',
                                    'style' => 'vertical',
                                    'label' => 'Textes du slideshow',
                                    'fields' => [
                                        '.initial_txt' => [
                                            'name' => 'texte_initial',
                                            'label' => 'Texte initial',
                                            'type' => 'select',
                                            'options' => [
                                                'SLIDER.INITIAL_TXT.OUR_ASSETS_ARE' => 'Nos atouts sont',
                                                'SLIDER.INITIAL_TXT.OUR_FORCES_ARE' => 'Nos forces sont'
                                            ]
                                        ],
                                        '.text' => [
                                            'name' => 'texte',
                                            'label' => 'Texte',
                                            'type' => 'editor'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
];
