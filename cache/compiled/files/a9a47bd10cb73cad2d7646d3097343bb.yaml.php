<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://error/error.yaml',
    'modified' => 1539949034,
    'data' => [
        'enabled' => true,
        'routes' => [
            404 => '/error'
        ]
    ]
];
