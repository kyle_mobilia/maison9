<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/home/clients/1b0a8e5a9f435d385ae4cefa98dfc9b7/web/user/plugins/admin/blueprints/admin/pages/move.yaml',
    'modified' => 1539949026,
    'data' => [
        'form' => [
            'validation' => 'loose',
            'fields' => [
                'route' => [
                    'type' => 'parents',
                    'label' => 'PLUGIN_ADMIN.PARENT',
                    'classes' => 'fancy'
                ]
            ]
        ]
    ]
];
