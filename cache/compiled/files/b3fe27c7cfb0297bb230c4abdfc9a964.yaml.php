<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/home/clients/1b0a8e5a9f435d385ae4cefa98dfc9b7/web/user/themes/maison9/blueprints/default.yaml',
    'modified' => 1539949042,
    'data' => [
        'extends@' => 'default',
        'form' => [
            'fields' => [
                'tabs' => [
                    'fields' => [
                        'advanced' => [
                            'fields' => [
                                'columns' => [
                                    'fields' => [
                                        'column1' => [
                                            'fields' => [
                                                'header.body_classes' => [
                                                    'markdown' => true,
                                                    'description' => ''
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        'metadata' => [
                            'type' => 'tab',
                            'title' => 'Meta Données',
                            'fields' => [
                                'header.meta_title' => [
                                    'type' => 'text',
                                    'label' => 'Meta Title'
                                ],
                                'header.meta_description' => [
                                    'type' => 'textarea',
                                    'label' => 'Meta description'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
];
