<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/home/clients/1b0a8e5a9f435d385ae4cefa98dfc9b7/web/system/blueprints/config/streams.yaml',
    'modified' => 1539949021,
    'data' => [
        'title' => 'PLUGIN_ADMIN.FILE_STREAMS',
        'form' => [
            'validation' => 'loose',
            'hidden' => true,
            'fields' => [
                'schemes.xxx' => [
                    'type' => 'array'
                ]
            ]
        ]
    ]
];
