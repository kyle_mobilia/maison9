<?php

/* landing-page.html.twig */
class __TwigTemplate_420e900cabc4d5710779d11f552ceaa5ffe4b392ad117662fa4062a9ee78d7ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'footer_javascript' => array($this, 'block_footer_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        ob_start();
        // line 2
        echo "    <!DOCTYPE html>
    <html lang=\"";
        // line 3
        echo (($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", array()), "getActive", array())) ? ($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", array()), "getActive", array())) : ($this->getAttribute($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "config", array()), "site", array()), "default_lang", array())));
        echo "\">
    <head>
        ";
        // line 5
        $this->displayBlock('head', $context, $blocks);
        // line 24
        echo "    </head>
    <body>
        <div class=\"slideshow\">
            ";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "slideshow", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["slide"]) {
            // line 28
            echo "                ";
            $context["media"] = $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), $this->getAttribute($context["slide"], "slide", array()), array(), "array"), "url", array());
            // line 29
            echo "                ";
            if ($this->getAttribute($context["slide"], "is_movie", array())) {
                // line 30
                echo "                    ";
                $context["preview"] = $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), $this->getAttribute($context["slide"], "movie_pictures", array()), array(), "array"), "url", array());
                // line 31
                echo "                    <video data-txt=\"";
                echo $this->getAttribute($context["slide"], "show_txt", array());
                echo "\" data-duration=\"";
                echo $this->getAttribute($context["slide"], "slide_duration", array());
                echo "\" class=\"slide movie\" loop muted autoplay poster=\"";
                echo ($context["preview"] ?? null);
                echo "\" style=\"background-color: ";
                echo $this->getAttribute($context["slide"], "movie_color", array());
                echo "\">
                        <source src=\"";
                // line 32
                echo ($context["media"] ?? null);
                echo "\" type=\"video/mp4\">
                    </video>
                ";
            } else {
                // line 35
                echo "                    <div data-txt=\"";
                echo $this->getAttribute($context["slide"], "show_txt", array());
                echo "\" data-duration=\"";
                echo $this->getAttribute($context["slide"], "slide_duration", array());
                echo "\" class=\"slide pictures\" style=\"background-image: url('";
                echo ($context["media"] ?? null);
                echo "');\"></div>
                ";
            }
            // line 37
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slide'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "        </div>
        <div class=\"slideshow-txt\">
            ";
        // line 40
        $context["count"] = 1;
        // line 41
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "slideshow_txt", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["txt"]) {
            // line 42
            echo "                <div id=\"txt-";
            echo ($context["count"] ?? null);
            echo "\" class=\"slide-txt\">
                    <span class=\"txt-top\">
                        <span>";
            // line 44
            echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->translate($this->getAttribute($context["txt"], "initial_txt", array()));
            echo "</span>
                    </span>
                    <span class=\"txt-effect\">
                        <noscript>";
            // line 47
            echo $this->getAttribute($context["txt"], "text", array());
            echo "</noscript>
                    </span>
                </div>
                ";
            // line 50
            $context["count"] = (($context["count"] ?? null) + 1);
            // line 51
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['txt'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "        </div>
        <div class=\"triangle\"></div>
        <div class=\"content\">
            <img src=\"";
        // line 55
        echo ($context["theme_url"] ?? null);
        echo "/images/logo.png\" />
            <a href=\"mailto:hello@maison9.ch\">hello@maison9.ch</a>
            <a href=\"tel:0041794629644\">+41 (0)79 462 96 44</a>
            <ul>
                <li><a target=\"_blank\" href=\"https://www.facebook.com/M9MarketingImmo\"><i class=\"fab fa-facebook-f\"></i></a></li>
                <li><a target=\"_blank\" href=\"https://www.instagram.com/maison9_marketing_vente_immo/\"><i class=\"fab fa-instagram\"></i></a></li>
            </ul>
        </div>
        ";
        // line 63
        $this->displayBlock('footer_javascript', $context, $blocks);
        // line 66
        echo "        ";
        echo $this->getAttribute(($context["assets"] ?? null), "js", array(), "method");
        echo "
    </body>
    </html>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "            <meta charset=\"utf-8\" />
            <title>";
        // line 7
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "meta_title", array());
        echo "</title>
            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
            <meta name=\"description\" content=\"";
        // line 10
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "meta_decription", array());
        echo "\">
            <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 11
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://images/favicon.png");
        echo "\" />
            <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.4.1/css/all.css\" integrity=\"sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz\" crossorigin=\"anonymous\">
            ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 16
        echo "            ";
        echo $this->getAttribute(($context["assets"] ?? null), "css", array(), "method");
        echo "
            ";
        // line 17
        $this->displayBlock('javascripts', $context, $blocks);
        // line 22
        echo "            ";
        echo $this->getAttribute(($context["assets"] ?? null), "js", array(), "method");
        echo "
        ";
    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 14
        echo "                ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", array(0 => "theme://css-compiled/style.css"), "method");
        // line 15
        echo "            ";
    }

    // line 17
    public function block_javascripts($context, array $blocks = array())
    {
        // line 18
        echo "                ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "jquery", 1 => 101), "method");
        // line 19
        echo "                ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/greensock/TweenMax.min.js"), "method");
        // line 20
        echo "                ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/typingEffect.js"), "method");
        // line 21
        echo "            ";
    }

    // line 63
    public function block_footer_javascript($context, array $blocks = array())
    {
        // line 64
        echo "            ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/slideshow.js"), "method");
        // line 65
        echo "        ";
    }

    public function getTemplateName()
    {
        return "landing-page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  220 => 65,  217 => 64,  214 => 63,  210 => 21,  207 => 20,  204 => 19,  201 => 18,  198 => 17,  194 => 15,  191 => 14,  188 => 13,  181 => 22,  179 => 17,  174 => 16,  172 => 13,  167 => 11,  163 => 10,  157 => 7,  154 => 6,  151 => 5,  141 => 66,  139 => 63,  128 => 55,  123 => 52,  117 => 51,  115 => 50,  109 => 47,  103 => 44,  97 => 42,  92 => 41,  90 => 40,  86 => 38,  80 => 37,  70 => 35,  64 => 32,  53 => 31,  50 => 30,  47 => 29,  44 => 28,  40 => 27,  35 => 24,  33 => 5,  28 => 3,  25 => 2,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% spaceless %}
    <!DOCTYPE html>
    <html lang=\"{{ grav.language.getActive ?: grav.config.site.default_lang }}\">
    <head>
        {% block head %}
            <meta charset=\"utf-8\" />
            <title>{{ page.header.meta_title}}</title>
            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
            <meta name=\"description\" content=\"{{ page.header.meta_decription }}\">
            <link rel=\"icon\" type=\"image/png\" href=\"{{ url('theme://images/favicon.png') }}\" />
            <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.4.1/css/all.css\" integrity=\"sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz\" crossorigin=\"anonymous\">
            {% block stylesheets %}
                {% do assets.addCss('theme://css-compiled/style.css') %}
            {% endblock %}
            {{ assets.css() }}
            {% block javascripts %}
                {% do assets.addJs('jquery', 101) %}
                {% do assets.addJs('theme://js/greensock/TweenMax.min.js') %}
                {% do assets.addJs('theme://js/typingEffect.js') %}
            {% endblock %}
            {{ assets.js() }}
        {% endblock head %}
    </head>
    <body>
        <div class=\"slideshow\">
            {% for slide in page.header.slideshow %}
                {% set media = page.media[slide.slide].url %}
                {% if slide.is_movie %}
                    {% set preview = page.media[slide.movie_pictures].url %}
                    <video data-txt=\"{{ slide.show_txt }}\" data-duration=\"{{ slide.slide_duration }}\" class=\"slide movie\" loop muted autoplay poster=\"{{ preview }}\" style=\"background-color: {{ slide.movie_color }}\">
                        <source src=\"{{ media }}\" type=\"video/mp4\">
                    </video>
                {% else %}
                    <div data-txt=\"{{ slide.show_txt }}\" data-duration=\"{{ slide.slide_duration }}\" class=\"slide pictures\" style=\"background-image: url('{{ media }}');\"></div>
                {% endif %}
            {% endfor %}
        </div>
        <div class=\"slideshow-txt\">
            {% set count = 1 %}
            {% for txt in page.header.slideshow_txt %}
                <div id=\"txt-{{ count }}\" class=\"slide-txt\">
                    <span class=\"txt-top\">
                        <span>{{ txt.initial_txt|t }}</span>
                    </span>
                    <span class=\"txt-effect\">
                        <noscript>{{ txt.text }}</noscript>
                    </span>
                </div>
                {% set count = count + 1 %}
            {% endfor %}
        </div>
        <div class=\"triangle\"></div>
        <div class=\"content\">
            <img src=\"{{ theme_url }}/images/logo.png\" />
            <a href=\"mailto:hello@maison9.ch\">hello@maison9.ch</a>
            <a href=\"tel:0041794629644\">+41 (0)79 462 96 44</a>
            <ul>
                <li><a target=\"_blank\" href=\"https://www.facebook.com/M9MarketingImmo\"><i class=\"fab fa-facebook-f\"></i></a></li>
                <li><a target=\"_blank\" href=\"https://www.instagram.com/maison9_marketing_vente_immo/\"><i class=\"fab fa-instagram\"></i></a></li>
            </ul>
        </div>
        {% block footer_javascript %}
            {% do assets.addJs('theme://js/slideshow.js') %}
        {% endblock %}
        {{ assets.js() }}
    </body>
    </html>
{% endspaceless %}", "landing-page.html.twig", "/Users/kylemobilia/Documents/MEO_WEBSITE_BITBUCKET/maison9/website/user/themes/maison9/templates/landing-page.html.twig");
    }
}
