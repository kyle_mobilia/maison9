<?php

/* partials/base.html.twig */
class __TwigTemplate_94090e18a8fbe549b4e924b5f4512d77fc48fcaffa61d39492f3d814256c332a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'content' => array($this, 'block_content'),
            'footer_javascript' => array($this, 'block_footer_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        ob_start();
        // line 2
        echo "    <!DOCTYPE html>
    <html lang=\"";
        // line 3
        echo (($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", array()), "getActive", array())) ? ($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", array()), "getActive", array())) : ($this->getAttribute($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "config", array()), "site", array()), "default_lang", array())));
        echo "\">
        <head>
            ";
        // line 5
        $this->displayBlock('head', $context, $blocks);
        // line 24
        echo "            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-NSCGTVM');</script>
            <!-- End Google Tag Manager -->
        </head>
        <body>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-NSCGTVM\"
                              height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
            ";
        // line 37
        echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "find", array(0 => "/configurations"), "method"), "header", array()), "googletag_head", array());
        echo "
            <header>

            </header>
            <div class=\"content\" role=\"main\">
                ";
        // line 42
        $this->displayBlock('content', $context, $blocks);
        // line 43
        echo "            </div>
            ";
        // line 44
        $this->displayBlock('footer_javascript', $context, $blocks);
        // line 46
        echo "            ";
        echo $this->getAttribute(($context["assets"] ?? null), "js", array(), "method");
        echo "
        </body>
    </html>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "                <meta charset=\"utf-8\" />
                <title>";
        // line 7
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "meta_title", array());
        echo "</title>
                <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
                <meta name=\"description\" content=\"";
        // line 10
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "meta_decription", array());
        echo "\">
                ";
        // line 11
        echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "find", array(0 => "/configurations"), "method"), "header", array()), "googletag_console", array());
        echo "
                <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 12
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://images/favicon.png");
        echo "\" />
                ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 16
        echo "                ";
        echo $this->getAttribute(($context["assets"] ?? null), "css", array(), "method");
        echo "
                ";
        // line 17
        $this->displayBlock('javascripts', $context, $blocks);
        // line 21
        echo "                ";
        echo $this->getAttribute(($context["assets"] ?? null), "js", array(), "method");
        echo "
                ";
        // line 22
        echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "find", array(0 => "/configurations"), "method"), "header", array()), "googletag_head", array());
        echo "googletag_console
            ";
    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 14
        echo "                    ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", array(0 => "theme://css-compiled/style.css"), "method");
        // line 15
        echo "                ";
    }

    // line 17
    public function block_javascripts($context, array $blocks = array())
    {
        // line 18
        echo "                    ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "jquery", 1 => array("priority" => 101, "loading" => "async")), "method");
        // line 19
        echo "                    ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/greensock/TweenMax.min.js", 1 => array("priority" => 102, "loading" => "async")), "method");
        // line 20
        echo "                ";
    }

    // line 42
    public function block_content($context, array $blocks = array())
    {
    }

    // line 44
    public function block_footer_javascript($context, array $blocks = array())
    {
        // line 45
        echo "            ";
    }

    public function getTemplateName()
    {
        return "partials/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 45,  148 => 44,  143 => 42,  139 => 20,  136 => 19,  133 => 18,  130 => 17,  126 => 15,  123 => 14,  120 => 13,  114 => 22,  109 => 21,  107 => 17,  102 => 16,  100 => 13,  96 => 12,  92 => 11,  88 => 10,  82 => 7,  79 => 6,  76 => 5,  66 => 46,  64 => 44,  61 => 43,  59 => 42,  51 => 37,  36 => 24,  34 => 5,  29 => 3,  26 => 2,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% spaceless %}
    <!DOCTYPE html>
    <html lang=\"{{ grav.language.getActive ?: grav.config.site.default_lang }}\">
        <head>
            {% block head %}
                <meta charset=\"utf-8\" />
                <title>{{ page.header.meta_title}}</title>
                <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
                <meta name=\"description\" content=\"{{ page.header.meta_decription }}\">
                {{ page.find('/configurations').header.googletag_console }}
                <link rel=\"icon\" type=\"image/png\" href=\"{{ url('theme://images/favicon.png') }}\" />
                {% block stylesheets %}
                    {% do assets.addCss('theme://css-compiled/style.css') %}
                {% endblock %}
                {{ assets.css() }}
                {% block javascripts %}
                    {% do assets.addJs('jquery', {'priority':101, 'loading':'async'}) %}
                    {% do assets.addJs('theme://js/greensock/TweenMax.min.js', {'priority':102, 'loading':'async'}) %}
                {% endblock %}
                {{ assets.js() }}
                {{ page.find('/configurations').header.googletag_head }}googletag_console
            {% endblock head %}
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-NSCGTVM');</script>
            <!-- End Google Tag Manager -->
        </head>
        <body>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-NSCGTVM\"
                              height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
            {{ page.find('/configurations').header.googletag_head }}
            <header>

            </header>
            <div class=\"content\" role=\"main\">
                {% block content %}{% endblock %}
            </div>
            {% block footer_javascript %}
            {% endblock %}
            {{ assets.js() }}
        </body>
    </html>
{% endspaceless %}", "partials/base.html.twig", "/home/clients/1b0a8e5a9f435d385ae4cefa98dfc9b7/web/user/themes/maison9/templates/partials/base.html.twig");
    }
}
