<?php

/* landing-page.html.twig */
class __TwigTemplate_b553ddd3ca1ee71250b9242dac20d538d119e464375158dab894a59596973a87 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'footer_javascript' => array($this, 'block_footer_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        ob_start();
        // line 2
        echo "    <!DOCTYPE html>
    <html lang=\"";
        // line 3
        echo (($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", array()), "getActive", array())) ? ($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", array()), "getActive", array())) : ($this->getAttribute($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "config", array()), "site", array()), "default_lang", array())));
        echo "\">
    <head>
        ";
        // line 5
        $this->displayBlock('head', $context, $blocks);
        // line 24
        echo "        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-NSCGTVM');</script>
        <!-- End Google Tag Manager -->
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-NSCGTVM\"
                          height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div class=\"slideshow\">
            ";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "slideshow", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["slide"]) {
            // line 39
            echo "                ";
            $context["media"] = $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), $this->getAttribute($context["slide"], "slide", array()), array(), "array"), "url", array());
            // line 40
            echo "                ";
            if ($this->getAttribute($context["slide"], "is_movie", array())) {
                // line 41
                echo "                    ";
                $context["preview"] = $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), $this->getAttribute($context["slide"], "movie_pictures", array()), array(), "array"), "url", array());
                // line 42
                echo "                    <video data-txt=\"";
                echo $this->getAttribute($context["slide"], "show_txt", array());
                echo "\" data-duration=\"";
                echo $this->getAttribute($context["slide"], "slide_duration", array());
                echo "\" class=\"slide movie\" loop muted autoplay poster=\"";
                echo ($context["preview"] ?? null);
                echo "\" style=\"background-color: ";
                echo $this->getAttribute($context["slide"], "movie_color", array());
                echo "\">
                        <source src=\"";
                // line 43
                echo ($context["media"] ?? null);
                echo "\" type=\"video/mp4\">
                    </video>
                ";
            } else {
                // line 46
                echo "                    <div data-txt=\"";
                echo $this->getAttribute($context["slide"], "show_txt", array());
                echo "\" data-duration=\"";
                echo $this->getAttribute($context["slide"], "slide_duration", array());
                echo "\" class=\"slide pictures\" style=\"background-image: url('";
                echo ($context["media"] ?? null);
                echo "');\">
                        <!--<img class=\"logo\" src=\"";
                // line 47
                echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), $this->getAttribute($context["slide"], "logo", array()), array(), "array"), "url", array());
                echo "\" />-->
                    </div>
                ";
            }
            // line 50
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slide'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "        </div>
        <div class=\"slideshow-txt\">
            ";
        // line 53
        $context["count"] = 1;
        // line 54
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "slideshow_txt", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["txt"]) {
            // line 55
            echo "                <div id=\"txt-";
            echo ($context["count"] ?? null);
            echo "\" class=\"slide-txt\">
                    <span class=\"txt-top\">
                        <span>";
            // line 57
            echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->translate($this->getAttribute($context["txt"], "initial_txt", array()));
            echo "</span>
                    </span>
                    <span class=\"txt-effect\">
                        <noscript>";
            // line 60
            echo $this->getAttribute($context["txt"], "text", array());
            echo "</noscript>
                    </span>
                </div>
                ";
            // line 63
            $context["count"] = (($context["count"] ?? null) + 1);
            // line 64
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['txt'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "        </div>
        <div class=\"triangle\"></div>
        <div class=\"content\">
            <img src=\"";
        // line 68
        echo ($context["theme_url"] ?? null);
        echo "/images/maison9_logo_rvb.svg\" />
            <a href=\"mailto:hello@maison9.ch\">hello@maison9.ch</a>
            <a href=\"tel:0041794629644\">+41 (0)79 462 96 44</a>
            <ul>
                <li><a target=\"_blank\" href=\"https://www.facebook.com/M9MarketingImmo\"><i class=\"fab fa-facebook-f\"></i></a></li>
                <li><a target=\"_blank\" href=\"https://www.instagram.com/maison9_marketing_vente_immo/\"><i class=\"fab fa-instagram\"></i></a></li>
            </ul>
        </div>
        ";
        // line 76
        $this->displayBlock('footer_javascript', $context, $blocks);
        // line 79
        echo "        ";
        echo $this->getAttribute(($context["assets"] ?? null), "js", array(), "method");
        echo "
    </body>
    </html>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "            <meta charset=\"utf-8\" />
            <title>";
        // line 7
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "meta_title", array());
        echo "</title>
            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
            <meta name=\"description\" content=\"";
        // line 10
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "meta_decription", array());
        echo "\">
            <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 11
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://images/favicon.png");
        echo "\" />
            <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.4.1/css/all.css\" integrity=\"sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz\" crossorigin=\"anonymous\">
            ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 16
        echo "            ";
        echo $this->getAttribute(($context["assets"] ?? null), "css", array(), "method");
        echo "
            ";
        // line 17
        $this->displayBlock('javascripts', $context, $blocks);
        // line 22
        echo "            ";
        echo $this->getAttribute(($context["assets"] ?? null), "js", array(), "method");
        echo "
        ";
    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 14
        echo "                ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", array(0 => "theme://css-compiled/style.css"), "method");
        // line 15
        echo "            ";
    }

    // line 17
    public function block_javascripts($context, array $blocks = array())
    {
        // line 18
        echo "                ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "jquery", 1 => 101), "method");
        // line 19
        echo "                ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/greensock/TweenMax.min.js"), "method");
        // line 20
        echo "                ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/typingEffect.js"), "method");
        // line 21
        echo "            ";
    }

    // line 76
    public function block_footer_javascript($context, array $blocks = array())
    {
        // line 77
        echo "            ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/slideshow.js"), "method");
        // line 78
        echo "        ";
    }

    public function getTemplateName()
    {
        return "landing-page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 78,  233 => 77,  230 => 76,  226 => 21,  223 => 20,  220 => 19,  217 => 18,  214 => 17,  210 => 15,  207 => 14,  204 => 13,  197 => 22,  195 => 17,  190 => 16,  188 => 13,  183 => 11,  179 => 10,  173 => 7,  170 => 6,  167 => 5,  157 => 79,  155 => 76,  144 => 68,  139 => 65,  133 => 64,  131 => 63,  125 => 60,  119 => 57,  113 => 55,  108 => 54,  106 => 53,  102 => 51,  96 => 50,  90 => 47,  81 => 46,  75 => 43,  64 => 42,  61 => 41,  58 => 40,  55 => 39,  51 => 38,  35 => 24,  33 => 5,  28 => 3,  25 => 2,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% spaceless %}
    <!DOCTYPE html>
    <html lang=\"{{ grav.language.getActive ?: grav.config.site.default_lang }}\">
    <head>
        {% block head %}
            <meta charset=\"utf-8\" />
            <title>{{ page.header.meta_title}}</title>
            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
            <meta name=\"description\" content=\"{{ page.header.meta_decription }}\">
            <link rel=\"icon\" type=\"image/png\" href=\"{{ url('theme://images/favicon.png') }}\" />
            <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.4.1/css/all.css\" integrity=\"sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz\" crossorigin=\"anonymous\">
            {% block stylesheets %}
                {% do assets.addCss('theme://css-compiled/style.css') %}
            {% endblock %}
            {{ assets.css() }}
            {% block javascripts %}
                {% do assets.addJs('jquery', 101) %}
                {% do assets.addJs('theme://js/greensock/TweenMax.min.js') %}
                {% do assets.addJs('theme://js/typingEffect.js') %}
            {% endblock %}
            {{ assets.js() }}
        {% endblock head %}
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-NSCGTVM');</script>
        <!-- End Google Tag Manager -->
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-NSCGTVM\"
                          height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div class=\"slideshow\">
            {% for slide in page.header.slideshow %}
                {% set media = page.media[slide.slide].url %}
                {% if slide.is_movie %}
                    {% set preview = page.media[slide.movie_pictures].url %}
                    <video data-txt=\"{{ slide.show_txt }}\" data-duration=\"{{ slide.slide_duration }}\" class=\"slide movie\" loop muted autoplay poster=\"{{ preview }}\" style=\"background-color: {{ slide.movie_color }}\">
                        <source src=\"{{ media }}\" type=\"video/mp4\">
                    </video>
                {% else %}
                    <div data-txt=\"{{ slide.show_txt }}\" data-duration=\"{{ slide.slide_duration }}\" class=\"slide pictures\" style=\"background-image: url('{{ media }}');\">
                        <!--<img class=\"logo\" src=\"{{ page.media[slide.logo].url }}\" />-->
                    </div>
                {% endif %}
            {% endfor %}
        </div>
        <div class=\"slideshow-txt\">
            {% set count = 1 %}
            {% for txt in page.header.slideshow_txt %}
                <div id=\"txt-{{ count }}\" class=\"slide-txt\">
                    <span class=\"txt-top\">
                        <span>{{ txt.initial_txt|t }}</span>
                    </span>
                    <span class=\"txt-effect\">
                        <noscript>{{ txt.text }}</noscript>
                    </span>
                </div>
                {% set count = count + 1 %}
            {% endfor %}
        </div>
        <div class=\"triangle\"></div>
        <div class=\"content\">
            <img src=\"{{ theme_url }}/images/maison9_logo_rvb.svg\" />
            <a href=\"mailto:hello@maison9.ch\">hello@maison9.ch</a>
            <a href=\"tel:0041794629644\">+41 (0)79 462 96 44</a>
            <ul>
                <li><a target=\"_blank\" href=\"https://www.facebook.com/M9MarketingImmo\"><i class=\"fab fa-facebook-f\"></i></a></li>
                <li><a target=\"_blank\" href=\"https://www.instagram.com/maison9_marketing_vente_immo/\"><i class=\"fab fa-instagram\"></i></a></li>
            </ul>
        </div>
        {% block footer_javascript %}
            {% do assets.addJs('theme://js/slideshow.js') %}
        {% endblock %}
        {{ assets.js() }}
    </body>
    </html>
{% endspaceless %}", "landing-page.html.twig", "/home/clients/1b0a8e5a9f435d385ae4cefa98dfc9b7/web/user/themes/maison9/templates/landing-page.html.twig");
    }
}
