<?php

/* partials/base.html.twig */
class __TwigTemplate_1daf3b934bf0783c9b49c839b15e0a19740a7eab33c820dc0a54f277bf11a809 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'content' => array($this, 'block_content'),
            'footer_javascript' => array($this, 'block_footer_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        ob_start();
        // line 2
        echo "    <!DOCTYPE html>
    <html lang=\"";
        // line 3
        echo (($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", array()), "getActive", array())) ? ($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", array()), "getActive", array())) : ($this->getAttribute($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "config", array()), "site", array()), "default_lang", array())));
        echo "\">
        <head>
            ";
        // line 5
        $this->displayBlock('head', $context, $blocks);
        // line 24
        echo "        </head>
        <body>
            ";
        // line 26
        echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "find", array(0 => "/configurations"), "method"), "header", array()), "googletag_head", array());
        echo "
            <header>

            </header>
            <div class=\"content\" role=\"main\">
                ";
        // line 31
        $this->displayBlock('content', $context, $blocks);
        // line 32
        echo "            </div>
            ";
        // line 33
        $this->displayBlock('footer_javascript', $context, $blocks);
        // line 35
        echo "            ";
        echo $this->getAttribute(($context["assets"] ?? null), "js", array(), "method");
        echo "
        </body>
    </html>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "                <meta charset=\"utf-8\" />
                <title>";
        // line 7
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "meta_title", array());
        echo "</title>
                <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
                <meta name=\"description\" content=\"";
        // line 10
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "meta_decription", array());
        echo "\">
                ";
        // line 11
        echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "find", array(0 => "/configurations"), "method"), "header", array()), "googletag_console", array());
        echo "
                <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 12
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://images/favicon.png");
        echo "\" />
                ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 16
        echo "                ";
        echo $this->getAttribute(($context["assets"] ?? null), "css", array(), "method");
        echo "
                ";
        // line 17
        $this->displayBlock('javascripts', $context, $blocks);
        // line 21
        echo "                ";
        echo $this->getAttribute(($context["assets"] ?? null), "js", array(), "method");
        echo "
                ";
        // line 22
        echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "find", array(0 => "/configurations"), "method"), "header", array()), "googletag_head", array());
        echo "googletag_console
            ";
    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 14
        echo "                    ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", array(0 => "theme://css-compiled/style.css"), "method");
        // line 15
        echo "                ";
    }

    // line 17
    public function block_javascripts($context, array $blocks = array())
    {
        // line 18
        echo "                    ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "jquery", 1 => array("priority" => 101, "loading" => "async")), "method");
        // line 19
        echo "                    ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/greensock/TweenMax.min.js", 1 => array("priority" => 102, "loading" => "async")), "method");
        // line 20
        echo "                ";
    }

    // line 31
    public function block_content($context, array $blocks = array())
    {
    }

    // line 33
    public function block_footer_javascript($context, array $blocks = array())
    {
        // line 34
        echo "            ";
    }

    public function getTemplateName()
    {
        return "partials/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 34,  137 => 33,  132 => 31,  128 => 20,  125 => 19,  122 => 18,  119 => 17,  115 => 15,  112 => 14,  109 => 13,  103 => 22,  98 => 21,  96 => 17,  91 => 16,  89 => 13,  85 => 12,  81 => 11,  77 => 10,  71 => 7,  68 => 6,  65 => 5,  55 => 35,  53 => 33,  50 => 32,  48 => 31,  40 => 26,  36 => 24,  34 => 5,  29 => 3,  26 => 2,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% spaceless %}
    <!DOCTYPE html>
    <html lang=\"{{ grav.language.getActive ?: grav.config.site.default_lang }}\">
        <head>
            {% block head %}
                <meta charset=\"utf-8\" />
                <title>{{ page.header.meta_title}}</title>
                <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
                <meta name=\"description\" content=\"{{ page.header.meta_decription }}\">
                {{ page.find('/configurations').header.googletag_console }}
                <link rel=\"icon\" type=\"image/png\" href=\"{{ url('theme://images/favicon.png') }}\" />
                {% block stylesheets %}
                    {% do assets.addCss('theme://css-compiled/style.css') %}
                {% endblock %}
                {{ assets.css() }}
                {% block javascripts %}
                    {% do assets.addJs('jquery', {'priority':101, 'loading':'async'}) %}
                    {% do assets.addJs('theme://js/greensock/TweenMax.min.js', {'priority':102, 'loading':'async'}) %}
                {% endblock %}
                {{ assets.js() }}
                {{ page.find('/configurations').header.googletag_head }}googletag_console
            {% endblock head %}
        </head>
        <body>
            {{ page.find('/configurations').header.googletag_head }}
            <header>

            </header>
            <div class=\"content\" role=\"main\">
                {% block content %}{% endblock %}
            </div>
            {% block footer_javascript %}
            {% endblock %}
            {{ assets.js() }}
        </body>
    </html>
{% endspaceless %}", "partials/base.html.twig", "/Users/kylemobilia/Documents/MEO_WEBSITE_BITBUCKET/maison9/website/user/themes/maison9/templates/partials/base.html.twig");
    }
}
