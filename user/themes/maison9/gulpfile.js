var gulp = require('gulp');
var sass = require('gulp-sass');
var cleancss = require('gulp-clean-css');
var csscomb = require('gulp-csscomb');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');

// configure the paths
var watch_dir = './scss/**/*.scss';
var src_dir = './scss/*.scss';
var dest_dir = './css-compiled';

var paths = {
    source: src_dir
};

var tingpng = require('gulp-tinypng');

var images = [
    'images/**/*.{png,jpg}',
    '../../pages/**/*.{png,jpg}'
];

gulp.task('image', function () {
    gulp.src(images, {base: '.'})
        .pipe(tingpng('ELK94xjwjDQHxH7a4jb7yPwxnfZ9eN7S')) // MEO design & communications
        .pipe(gulp.dest('.'));
});

gulp.task('watch', function() {
    gulp.watch(watch_dir, ['build']);
});

gulp.task('build', function() {
    gulp.src(paths.source)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compact', precision: 10})
            .on('error', sass.logError)
        )
        .pipe(sourcemaps.write())
        .pipe(autoprefixer())
        .pipe(gulp.dest(dest_dir))
        .pipe(csscomb())
        .pipe(cleancss())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(dest_dir));
});

gulp.task('default', ['build']);
