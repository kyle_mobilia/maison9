/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function(){

    var stopScript = false;
    var currentSlideTxt = 0;

    /* SLIDE SHOW IMAGES */
    var slides = $(".slideshow .slide");
    var currentSlide = 0;
    var stayTime = slides.attr('data-duration');
    var showTxt = slides.attr('data-txt');
    var slideTime = 1;
    showSlideText(showTxt);
    TweenLite.set(slides.filter(":gt(0)"), {scale: 1, autoAlpha:0});
    TweenLite.delayedCall(stayTime, nextSlide);
    function nextSlide()
    {
        TweenLite.to( slides.eq(currentSlide), slideTime, {scale: 1, autoAlpha:0} );
        currentSlide = ++currentSlide % slides.length;
        TweenLite.to( slides.eq(currentSlide), slideTime, {scale: 1, autoAlpha:1} );
        stayTime = slides.eq(currentSlide).attr('data-duration');
        showTxt  = slides.eq(currentSlide).attr('data-txt');
        showSlideText(showTxt);
        TweenLite.delayedCall(stayTime, nextSlide);
    }
    /* SLIDE SHOW IMAGES */

    /* SLIDE SHOW TXT */

    function showTxtF() {
        if(stopScript == false){
            var slidesMaster = $(".slideshow-txt");
            var totalSlide = slidesMaster.find('.slide-txt').length;
            console.log('BEFORE | '+currentSlideTxt);
            if(currentSlideTxt >= totalSlide) {
                currentSlideTxt = 1;
            }else{
                currentSlideTxt += 1;
            }
            console.log('AFTER | '+currentSlideTxt);
            //var randomNextSlideTxt = Math.floor((Math.random() * totalSlide) + 1);
            var slide = slidesMaster.find('#txt-'+currentSlideTxt);
            slide.show();
            slide.find('.txt-top span').animate({top:0}, 400, function() {
                typeWrite(slide.find('.txt-effect'));
            });
        }
    }

    /* SLIDE SHOW TXT */

    function showSlideText(showTxt) {
        if(showTxt == 1) {
            $('.slideshow-txt').show();
            if(stopScript == true) {
                stopScript = false;
                showTxtF();
            }
        }else{
            $('.slideshow-txt').hide();
            stopScript = true;
        }
    }

    function randomIntFromInterval(min,max)
    {
        return Math.floor(Math.random()*(max-min+1)+min);
    }

    function typeWrite(span){
        span.addClass('cursor')
        var text = span.text();
        var randInt = 0
        for (var i = 0; i < text.length; i++) {
            randInt += parseInt(randomIntFromInterval(30,250));
            if(stopScript == false) {
                var typing = setTimeout(function (y) {
                    span.append(text.charAt(y));
                }, randInt, i);
            }else{
                span.html('<noscript>'+text+'</noscript>');
                showTxtF();
            }
        };
        setTimeout(function(){
            for (var i = text.length; i > -1; i--) {
                randInt += parseInt(randomIntFromInterval(10,50));
                var typing = setTimeout(function(y){
                    span.text(text.substring(0,y));
                },randInt, i);
            };
            setTimeout(function(){
                span.parent().find('.txt-top span').animate({top:'2.5rem'}, 400, function() {
                    span.parent().hide();
                    span.html('<noscript>'+text+'</noscript>');
                    showTxtF();
                });
            },randInt+200);
        },2500);
    }

});
