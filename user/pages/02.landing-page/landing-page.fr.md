---
title: 'Landing Page'
media_order: 'screenshot_video.jpg,LANDING PAGE_1_HKR.mp4,1.jpg,2.jpg,3.jpg,5.jpg,8.jpg,12.jpg,13.jpg,14.jpg,15.jpg,16.jpg,17.jpg,18.jpg,19.jpg,20.jpg,21.jpg,22.jpg,23.jpg,25.jpg,carre_vert_logo_rvb.svg,dally_logo_horizontal_rvb.svg,gefiswiss_logo_vertical_rvb.svg,gefiswiss_lpdl_logo_rvb.svg,hedonia_bronze_logo_rvb.svg,pierre_etoile_rvb.svg,steiner_logo_rvb.svg,24.jpg,26.jpg,26_B.jpg'
slideshow:
    -
        slide: 'LANDING PAGE_1_HKR.mp4'
        slide_duration: 11
        show_txt: '0'
        is_movie: '1'
        movie_color: '#fee692'
        movie_pictures: screenshot_video.jpg
    -
        slide: 1.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: gefiswiss_logo_vertical_rvb.svg
    -
        slide: 2.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: gefiswiss_logo_vertical_rvb.svg
    -
        slide: 3.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: pierre_etoile_rvb.svg
    -
        slide: 5.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: steiner_logo_rvb.svg
    -
        slide: 8.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: dally_logo_horizontal_rvb.svg
    -
        slide: 12.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: carre_vert_logo_rvb.svg
    -
        slide: 13.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: carre_vert_logo_rvb.svg
    -
        slide: 14.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: carre_vert_logo_rvb.svg
    -
        slide: 15.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: carre_vert_logo_rvb.svg
    -
        slide: 16.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: hedonia_bronze_logo_rvb.svg
    -
        slide: 17.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: hedonia_bronze_logo_rvb.svg
    -
        slide: 18.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: pierre_etoile_rvb.svg
    -
        slide: 19.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: dally_logo_horizontal_rvb.svg
    -
        slide: 20.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: dally_logo_horizontal_rvb.svg
    -
        slide: 21.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: gefiswiss_lpdl_logo_rvb.svg
    -
        slide: 22.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: gefiswiss_lpdl_logo_rvb.svg
    -
        slide: 23.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: pierre_etoile_rvb.svg
    -
        slide: 24.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: hedonia_bronze_logo_rvb.svg
    -
        slide: 25.jpg
        slide_duration: 5
        show_txt: '1'
        is_movie: '0'
        logo: hedonia_bronze_logo_rvb.svg
    -
        slide: 26_B.jpg
        slide_duration: 5
        show_txt: '0'
        is_movie: '0'
slideshow_txt:
    -
        initial_txt: SLIDER.INITIAL_TXT.OUR_FORCES_ARE
        text: 'Créer de la personnalité'
    -
        initial_txt: SLIDER.INITIAL_TXT.OUR_FORCES_ARE
        text: 'Les solutions innovantes'
    -
        initial_txt: SLIDER.INITIAL_TXT.OUR_FORCES_ARE
        text: 'Le stewarding: accompagnement ventes'
    -
        initial_txt: SLIDER.INITIAL_TXT.OUR_FORCES_ARE
        text: 'Le marketing immobilier'
    -
        initial_txt: SLIDER.INITIAL_TXT.OUR_ASSETS_ARE
        text: 'L’ efficacité, la simplicité, la pertinence'
meta_title: 'Maison 9 | marketing immobilier nouvelle génération et ventes'
meta_description: 'Maison 9 est une agence qui rendra votre projet immobilier attractif en optimisant sa visibilité grâce à l''expertise combinée de spécialistes en marketing et de l''immobilier.'
---

